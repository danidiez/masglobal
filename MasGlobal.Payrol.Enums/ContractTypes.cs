﻿using System;

namespace MasGlobal.Payrol.Enums
{
    public enum ContractTypes
    {
        HourlySalaryEmployee = 1,
        MonthlySalaryEmployee = 2
    }
}
