using MasGlobal.Payrol.Business;
using MasGlobal.Payrol.Business.Interface;
using MasGlobal.Payrol.Dao;
using MasGlobal.Payrol.Dao.Interface;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace MasGlobal.Payrol
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<ISalaryCalculationBs, SalaryCalculationBs>();
            container.RegisterType<IApiEmployeesDao, ApiEmployeesDao>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}