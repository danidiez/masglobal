﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace MasGlobal.Payrol.Api.Contracts
{
    public interface IEmployeeController
    {
        IHttpActionResult GetEmployees(int? employeeId = null);
    }
}
