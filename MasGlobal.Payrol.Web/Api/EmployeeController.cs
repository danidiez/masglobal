﻿using MasGlobal.Payrol.Api.Contracts;
using MasGlobal.Payrol.Business;
using MasGlobal.Payrol.Business.Interface;
using MasGlobal.Payrol.Dto;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace MasGlobal.Payrol.Api
{
    public class EmployeeController : ApiController, IEmployeeController
    {
        
        private ISalaryCalculationBs salaryCalculationBs;

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public EmployeeController() {
        }

        public EmployeeController(ISalaryCalculationBs salaryCalculationBs)
        {
            this.salaryCalculationBs = salaryCalculationBs;
        }

        [ResponseType(typeof(IEnumerable<EmployeeDto>))]
        [HttpGet]
        public IHttpActionResult GetEmployees(int? employeeId = null)
        {
            try
            {
                salaryCalculationBs = new SalaryCalculationBs();
                var result = salaryCalculationBs.GetEmployees(employeeId);
                return Ok(result);
            }
            catch(Exception ex)
            {
               return BadRequest(string.Format("An error ahs ocurred: {0}", ex.Message));
            }            
        }
    }
}
