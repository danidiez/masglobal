﻿using MasGlobal.Payrol.Business;
using MasGlobal.Payrol.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MasGlobal.Payrol.Test
{
    [TestClass]
    public class SalaryCalculationBsTest
    {
        [TestMethod]
        public void MonthlySalaryEmployeeCorrectly()
        {

            SalaryCalculationBs salaryCalculationBs = new SalaryCalculationBs();
            EmployeeDto emp = new EmployeeDto
            {
                Id = 1,
                ContractTypeName = "MonthlySalaryEmployee",
                MonthlySalary = 1000
            };
            double anualSalary = salaryCalculationBs.CalculateAnualSalary(emp);
            Assert.AreEqual(anualSalary, 12000);
        }

        [TestMethod]
        public void MonthlySalaryEmployeeIncorrect()
        {

            SalaryCalculationBs salaryCalculationBs = new SalaryCalculationBs();
            EmployeeDto emp = new EmployeeDto
            {
                Id = 1,
                ContractTypeName = "MonthlySalaryEmployee",
                MonthlySalary = 1000
            };
            double anualSalary = salaryCalculationBs.CalculateAnualSalary(emp);
            Assert.AreNotEqual(anualSalary, 1000);
        }
    }
}
