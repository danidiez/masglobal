﻿using MasGlobal.Payrol.Business.Interface;
using MasGlobal.Payrol.Dao;
using MasGlobal.Payrol.Dao.Interface;
using MasGlobal.Payrol.Dto;
using MasGlobal.Payrol.Enums;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace MasGlobal.Payrol.Business
{
    public class SalaryCalculationBs : ISalaryCalculationBs 
    {

        private IApiEmployeesDao apiEmployeesDao;

        public SalaryCalculationBs() {
        }

        public SalaryCalculationBs(IApiEmployeesDao apiEmployeesDao)
        {
            this.apiEmployeesDao = apiEmployeesDao;
        }

        public IEnumerable<EmployeeDto> GetEmployees(int? employeeId)
        {
            apiEmployeesDao = new ApiEmployeesDao();
            //getting the serialized object
            var jsonData = apiEmployeesDao.GetEmployees();
            //deserializing the object
            JavaScriptSerializer js = new JavaScriptSerializer();
            var employees = js.Deserialize<List<EmployeeDto>>(jsonData);
            List<EmployeeDto> employeesResult;

            if (!employeeId.HasValue)
            {
                employeesResult = employees;
            }
            else
            {
                employeesResult = employees.FindAll(x => x.Id == employeeId);
            }

            //Calculating anual salary
            foreach (EmployeeDto employee in employeesResult)
            {
                employee.AnualSalary = CalculateAnualSalary(employee);
            }

            return employeesResult;
        }

        /// <summary>
        /// Calculate the anual salary based on contract type
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public double CalculateAnualSalary(EmployeeDto employee)
        {
            //This method should be private, it was created public for unit test purposes
            double anualSalary = 0;

            if (employee.ContractTypeName == ContractTypes.HourlySalaryEmployee.ToString())
            {
                anualSalary = 120 * employee.HourlySalary * 12;
            }
            else if (employee.ContractTypeName == ContractTypes.MonthlySalaryEmployee.ToString())
            {
                anualSalary = employee.MonthlySalary * 12;
            }

            return anualSalary;
        }
    }
}
