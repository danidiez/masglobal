﻿using MasGlobal.Payrol.Dto;
using System.Collections.Generic;

namespace MasGlobal.Payrol.Business.Interface
{
    public interface ISalaryCalculationBs
    {
        IEnumerable<EmployeeDto> GetEmployees(int? employeeId);

        double CalculateAnualSalary(EmployeeDto employee);
    }
}
